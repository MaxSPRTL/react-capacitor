import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.maxsprtl.mune',
  appName: 'Mune',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
