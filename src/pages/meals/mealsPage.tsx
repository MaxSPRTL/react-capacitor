import { AddMealForm } from "components/meals/addMeal/addMeal";
import { MealsList } from "components/meals/mealsList/mealsList";
import "pages/meals/mealsPage.scss";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { MealsActions } from "store/modules/meals/mealsActions";

export const MealsPage = () => {
  const dispatch = useAppDispatch()
  const meals = useAppSelector((state) => state.mealsReducer.meals)

  useEffect(() => {
    (async () => {
      await dispatch(MealsActions.fetchMeals())
    })()
  }, [])

  return (
    <div className="meals-page">
      <div className="meals-page__add">
        <AddMealForm />
      </div>
      {
        Array.isArray(meals) && meals.length > 0 ?
          <MealsList meals={meals} />
          : null
      }
    </div>
  )
}