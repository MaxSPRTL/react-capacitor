import "components/inputs/input.scss";
import { useEffect, useState } from "react";

export interface IPropsInput {
  type?: string,
  name?: string,
  defaultValue?: string,
  placeholder?: string,
  onValueChange?: Function,
  onEnterKeyDown?: Function
}

export const Input = (props: IPropsInput) => {
  const [inputValue, setInputValue] = useState(props.defaultValue || '')

  useEffect(() => { updateDefaultValue() }, [props.defaultValue])
  useEffect(() => { emitInputValueChange() }, [inputValue])

  const updateDefaultValue = () => {
    setInputValue(props.defaultValue || '')
  }

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value)
  }

  const emitInputValueChange = () => {
    if (props.onValueChange) {
      props.onValueChange(inputValue)
    }
  }

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      if (props.onEnterKeyDown) {
        props.onEnterKeyDown(inputValue)
      }
    }
  }

  return (
    <input
      className="c-input"
      type={props.type}
      name={props.name}
      value={inputValue}
      placeholder={props.placeholder}
      onChange={handleInputChange}
      onKeyDown={handleKeyDown}
    />
  )
}