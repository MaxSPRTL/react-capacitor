import { Button } from "components/buttons/button";
import { Icon, ICON_NAMES } from "components/icons/icon";
import { Input } from "components/inputs/input";
import "components/meals/addMeal/addMeal.scss";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useAppDispatch } from "store/hooks";
import { MealsActions } from "store/modules/meals/mealsActions";

export const AddMealForm = () => {
  const dispatch = useAppDispatch()

  const [name, setName] = useState<string>('')

  const { t } = useTranslation()

  const handleInputChange = (newValue: string) => {
    setName(newValue)
  }

  const handleCancelBtn = () => {
    clearForm()
  }

  const handleSubmitBtn = async () => {
    dispatch(MealsActions.createMeal(name))
    await dispatch(MealsActions.saveMeals())
    clearForm()
  }

  const clearForm = () => {
    setName('')
  }

  const handleNameInputEnter = (newValue: string) => {
    setName(newValue)
    handleSubmitBtn()
  }

  return (
    <div className="add-meal">
      <div className="add-meal__input">
        <Input
          onValueChange={handleInputChange}
          placeholder={t('meals.form.addAMeal')}
          defaultValue={name}
          onEnterKeyDown={handleNameInputEnter}
        />
      </div>
      <div className="add-meal__actions">
        <div className="actions__cancel">
          <Button
            icon={<Icon iconName={ICON_NAMES.CROSS} iconSize="28" />}
            onClick={handleCancelBtn}
          />
        </div>
        <div className="actions__submit">
          <Button
            icon={<Icon iconName={ICON_NAMES.PLUS} iconSize="28" />}
            onClick={handleSubmitBtn}
          />
        </div>
      </div>
    </div>
  )
}