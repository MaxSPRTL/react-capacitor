import "components/meals/mealsList/mealsList.scss";
import { MealsListItem } from "components/meals/mealsListItem/mealsListItem";
import React from "react";
import { IMeal } from "types/meals/IMeal";

export interface IMealsListProps {
  meals: IMeal[]
}

export class MealsList extends React.Component<IMealsListProps> {
  render(): React.ReactNode {
    return (
      <div className="meals-list">
        {
          this.props.meals.map((meal: IMeal) =>
            <div key={meal.id} className="meals-list__item">
              <MealsListItem meal={meal} />
            </div>
          )
        }
      </div>
    )
  }
}