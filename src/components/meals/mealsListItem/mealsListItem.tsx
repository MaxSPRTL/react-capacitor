import { Button } from "components/buttons/button";
import { Icon, ICON_NAMES } from "components/icons/icon";
import "components/meals/mealsListItem/mealsListItem.scss";
import { useAppDispatch } from "store/hooks";
import { MealsActions } from "store/modules/meals/mealsActions";
import { IMeal } from "types/meals/IMeal";

export interface IPropsMealsListItem {
  meal?: IMeal
}

export const MealsListItem = (props: IPropsMealsListItem) => {
  const { meal } = props
  const dispatch = useAppDispatch()

  const handleDeleteBtn = async () => {
    await handleMealDeletion()
  }

  const handleMealDeletion = async () => {
    if (meal !== undefined) {
      dispatch(MealsActions.deleteMeal(meal.id))
      await dispatch(MealsActions.saveMeals())
    }
  }

  if (meal !== undefined) {
    return (
      <div className="meal">
        <div className="meal__name">{meal.name}</div>
        <div className="actions">
          <div className="actions__edit">
            <Button icon={<Icon iconName={ICON_NAMES.EDIT} iconSize="28" />} />
          </div>
          <div className="actions__delete">
            <Button
              icon={<Icon iconName={ICON_NAMES.TRASH} iconSize="28" />}
              onClick={handleDeleteBtn}
            />
          </div>
        </div>
      </div>
    )
  } else {
    return null
  }
}