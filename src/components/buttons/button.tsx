import "components/buttons/button.scss";

export interface IPropsButton {
  icon?: React.ReactNode,
  text?: string,
  onClick?: Function
}

export const Button = (props: IPropsButton) => {
  const handleButtonClick = () => {
    if (props.onClick) {
      props.onClick()
    }
  }

  return (
    <button
      className="c-btn c-btn--ripple"
      onClick={handleButtonClick}
    >
      {
        props.icon ?
          <div className="c-btn__icon">
            {props.icon}
          </div>
          : null
      }
      {
        props.text ?
          <div className="c-btn__text">
            {props.text}
          </div>
          : null
      }
    </button>
  )
}