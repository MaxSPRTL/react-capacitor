import { AlertOptions, ConfirmOptions, Dialog, PromptOptions } from '@capacitor/dialog';

export namespace DialogService {
  export const showAlert = async (options: AlertOptions) => {
    await Dialog.alert({ ...options })
  }

  export const showConfirm = async (options: ConfirmOptions) => {
    const { value } = await Dialog.confirm({ ...options })
    return value
  }

  export const showPrompt = async (options: PromptOptions) => {
    const { value, cancelled } = await Dialog.prompt({ ...options })
    return { value, cancelled }
  }
}

