import { ShowOptions, Toast } from '@capacitor/toast';

export namespace ToastService {
  export const displayToast = async (toastOptions: ShowOptions) => {
    await Toast.show({ ...toastOptions })
  }
}